# WASI Intermission

## Introduction to WASM and WASI

WASM or [WebAssembly](https://webassembly.org/) is a binary instruction format for an OS independent
stack-based virtual machine. Wasm is designed as a portable compilation target for multiple programming languages,
enabling deployment on the web for client and server applications. Currently, commonly used programming languages for
WebAssembly are Rust, C++ and Go. A Wasm application itself only has access to a fixed linear memory, without any
possibility to access for example the file system of the host system. Security wise this is a very interesting and
important aspect of WebAssembly.

When running outside the browser and for more complex applications we may need a way to use functionality of the host:
timing, http, file system etc. This is where WASI comes into play.

WASI is the [WebAssembly System Interface](https://wasi.dev/).
This means that in a WebAssembly executable a set of functionalities is available that are executed by the WASM runtime
directly on the host system. Different WebAssembly runtimes provide these functionalities on different host systems.
There are also differences in the quantity of functions runtimes provide beside of the official WASI interface (e.g. see
Fermyon + Spin).

<p align="center">
    <img width="60%" src="assets/WASI+WASM.png">
</p>

## Overview of WASM runtimes

An extensive list of WASM runtimes can be found [here](https://github.com/appcypher/awesome-wasm-runtimes).
During the intermission we worked with the following:

* Wasmtime
    * Used by krustlet, Spin
    * Implements WASI interface and has the
      following [additional features](https://docs.rs/wasmtime-wasi/latest/wasmtime_wasi/)
    * Supports the following [features via sdk](https://github.com/fermyon/wasi-experimental-toolkit)

* WasmEdge
    * Implements WASI interface and has the
      following [additional features](https://wasmedge.org/book/en/intro/standard.html)

* Wasmer

## How to build and distribute WASI binaries?

Building Wasm/Wasi binaries works exactly like building other binaries, but with a different compilation target. For
example in Rust

```shell
cargo build
```

will generate a normal executable while

```shell
cargo build --target wasm32-wasi
```

will generate a `.wasm` binary file.

One possibility to distribute `.wasm` files is by pushing them to OCI registries that support images other that docker
images. We were able to implement
a [CI pipeline in a Gitlab project](https://gitlab.com/wasm-intermission-senacor/krustlet-bin/-/tree/main) that pushed
the generated binaries to the GitLab registry which were then downloadable and deployable to e.g. k8s via krustlet.

## How to deploy a WASI module?

As this is a workload used outside the browser we are talking about WASI modules

* k8s
    * [krustlet](https://docs.krustlet.dev)

      Run wasm workloads as an OCI image. Krustlet acts as a k8s plugin and adds a new node with the default
      runtime `wasmtime`. [Example application](https://gitlab.com/wasm-intermission-senacor/krustlet-bin/-/tree/main).

    * [WasmEdge with runtime](https://wasmedge.org/book/en/kubernetes/quickstart.html)

      Not tested
    * Wasm in Container -> Docker Container with WASM Runtime

      Not recommended as you lose most of the features you would seek to gain by using WASM directly

* Fermyon
    * [Spin](https://github.com/fermyon/spin)
        * Run wasm modules as functions. They are triggered by the Spin/Fermyon runtime
        * Features: Redis trigger + outgoing, HTTP trigger + outgoing, pg db outgoing (experimental)
        * Currently not really usable in production as there are features missing for a good infrastructure -> No
          scaling i.e. worker nodes possible. Announced to be available in the future
        * [Example application](https://gitlab.com/wasm-intermission-senacor/fermyon-spin-demo)

* WASI modules as serverless functions
    * [Cloudflare workers](https://workers.cloudflare.com)
        * First class support for WASM workloads instantly deployed globally. The whole thing is the very definition of
          serverless deployment. No functionality for database connections. The workers do have access to a CloudFlare
          specific kv store and static assets. Free tier has 100,000 requests per day and an execution time of 10ms
        * [Announcement](https://blog.cloudflare.com/workers-rust-sdk/)
        * Most extensive documentation together with Fastly.

    * [Second State](secondstate.io)
        * First class support for writing serverless functions in Rust via WASM and interoperability with Node
        * [Support for HTTP](https://www.secondstate.io/articles/internet-of-functions-http-proxy/), interoperability
          with Node and support for Tensorflow

    * [Fastly](https://docs.fastly.com/products/compute-at-edge)
        * First class support for Rust and js services compiled to wasm. Experimental support for Go. Functionalities
          include logging, http calls and reading and writing to a kv store from Fastly. All this depends heavily on the
          fastly_sdk crate. Fastly uses the in-house developed WASM runtime called Lucid.
        * Most extensive documentation together with Cloudflare

## Can I confidently write a WASI microservice for production?

Before writing a microservice with a certain tool stack I have to check at least if the tool stack

1. Offers all necessary functionalities that are needed to fulfil the requested use cases

2. Offers possibilities to monitor the running service to verify that it is working as expected and investigate possible
   errors.

We have examined WASI itself as well as dedicated frameworks as Fermyon/Spin and WASM runtimes like Wasmer and Wasmtime
for these capabilities.

#### Ad 1.

Taking a look at the current state of WASI reveals that fundamental requirements for a modern Microservice are not
existing yet:https://github.com/WebAssembly/WASI/blob/main/Proposals.md. No database access, no clear http handling, ...
Though the WASM runtimes have not only implemented the existing WASI interface, but have furthermore added certain
functionalities on their own. The extended interface that is used by Fermyon can be found
here: https://github.com/fermyon/wasi-experimental-toolkit
These http functionalities work as expected and connote that at least basic microservices would be implementable. It has
to be noted that these interfaces are experimental and can change if the WASI standard develops in another direction.

#### Ad 2.

The current WASI standard is not offering any monitoring possibilities at all. Fermyon adds logging capabilities but no
further monitoring tools.
It is imaginable that certain parts of monitoring (like measuring database and http response times) do not have to be
part of WASI, because it can be part of the runtime. Though there might be the need to measure other business metrics
inside a WASM module or even metrics about the module itself. This part is completely missing until now.

#### Resumé

It is possible to write very basic WASI Microservices right now. But since they are based on experimental APIs and there
are nearly no monitoring capabilities at all, it can be said very clearly that it is not possible right now to bring a
WASI Microservice confidently to production.

## Possible use cases for WASI/WASM modules

* Writing functionality that needs to be executable on various platforms and systems. This includes core functionalities
  that could be packaged and used as libraries in other programming languages.
* Writing functionality for infrastructure on which resource consumption is crucial and where a virtualization layer
  like docker might already be overkill or too much overhead for your application
* In need of fast scaling and short start up.

## Rust binary in Docker vs WASM binary

Abstraction layer of docker adds overhead. Just the binary size alone between an unoptimised docker-packaged rust binary (467k) and a
WASM binary (80k) is significant if working with high-speed workloads. We didn't do any real benchmark in terms of
performance, but it is expected that the Rust binary performs equally well, if not better because of the
larger/specialised instruction set available to the compiler and subsequently the processor.